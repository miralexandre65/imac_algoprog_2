#include "tp6.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;

// **adjancies veut dire que c'est un tableau de tableau
void Graph::buildFromAdjenciesMatrix(int **adjacencies, int nodeCount)
{
	/**
	  * Make a graph from a matrix
	  * first create all nodes, add it to the graph then connect them
	  * this->appendNewNode
	  * this->nodes[i]->appendNewEdge
	  */
    for (int i = 0; i<nodeCount; i++) {
        GraphNode* node = new GraphNode(i);
        this->appendNewNode(node);
    }

    for (int i = 0; i<nodeCount; i++) {
        for (int j = 0; j<nodeCount; j++) {
            if (adjacencies[i][j] != 0) {
                this->nodes[i]->appendNewEdge(this->nodes[j], adjacencies[i][j]);
            }
        }
    }
}

//à certains moments le programme plante, mais l'erreur spécifie un malloc quelque part, donc je suppose que ça ne vient pas de mon programme, car sur de petits graphes cela fonctionne
void Graph::deepTravel(GraphNode *first, GraphNode *nodes[], int &nodesSize, bool visited[])
{
	/**
	  * Fill nodes array by travelling graph starting from first and using recursivity
      * visited est rempli de false au départ
	  */
    visited[first->value] = true;
        //e->next fait passer du premier lien au deuxième lien et pas d'un sommet à l'autre
    //le cas d'arrêt c'est le cas où e == nullptr
    for (Edge* e = first->edges; e != nullptr; e = e->next) {
        if (!visited[e->destination->value]) {
            nodes[nodesSize] = e->source;
            nodesSize++;
            this->deepTravel(e->destination, nodes, nodesSize, visited);
        }
    }
}


bool Graph::detectCycle(GraphNode *first, bool visited[])
{
	/**
	  Detect if there is cycle when starting from first
	  (the first may not be in the cycle)
	  Think about what's happen when you get an already visited node
	**/
    //en gros le but est de déterminer s'il y a un cycle dans la composante connexe de first
    visited[first->value] = true;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 150;
	w = new GraphWindow();
	w->show();

	return a.exec();
}
