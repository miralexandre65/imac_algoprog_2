#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

uint max(int a, int b) {
    //renvoie le max entre a et b
    if (a>b) {
        return a;
    }
    else {
        return b;
    }
} 

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        // on considère que les arbres passés en argument existent
        if (value > this->value) {
            if (this->right) {
                this->right->insertNumber(value);
            }
            else {
                SearchTreeNode* node = new SearchTreeNode(value);
                this->right = node;
            }
        }
        else {
            if (this->left) {
                this->left->insertNumber(value);
            }
            else {
                SearchTreeNode* node = new SearchTreeNode(value);
                this->left = node;
            }
        }
    }

    //le const est là pour dire que l'on ne modifie pas l'arbre sur lequel on applique la fonction
	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        // le seul cas d'arrêt est si on a une feuille, sinon on a 3 cas différents à étudier
        if (this->isLeaf()) {
            return 1;
        }
        else if (this->left && !this->right) {
            return 1 + this->left->height();
        }
        else if (!this->left && this->right) {
            return 1 + this->right->height();
        }
        else {
            return 1 + max(this->right->height(), this->left->height());
        }
    }

    uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        if (this->isLeaf()) {
            return 1;
        }
        else if (this->left && !this->right) {
            return 1 + this->left->nodesCount();
        }
        else if (!this->left && this->right) {
            return 1 + this->right->nodesCount();
        }
        else {
            return 1 + this->right->nodesCount() + this->left->nodesCount();
        }
    }

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if (!this->left && !this->right) {
            return true;
        }
        else {
            return false;
        }
	}

    //les références, c'est comme des pointeurs, sauf que c'est du c++ et pas du c, et que c'est plus facile à manipuler, il vaut mieux utiliser ça, c'est mieux
	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if (this->isLeaf()) {
            leaves[leavesCount] = this;
            leavesCount++;
        }
        else {
            if (this->right) {
                this->right->allLeaves(leaves, leavesCount);
            }
            if (this->left) {
                this->left->allLeaves(leaves, leavesCount);
            }
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if (this->isLeaf()) {
            nodes[nodesCount] = this;
            nodesCount++;
        }
        //dans ce programme le cas d'arrêt est une feuille ou s'il n'y a pas de fils gauche ou droit on ne fait juste rien et on ajoute seulement le parent. Ensuite il faut mettre les étapes dans le bon ordre en commençant par le fils gauche, le parent et le fils droit.
        else {
            if (this->left) {
                this->left->inorderTravel(nodes, nodesCount);
            }
            //peut être mettre un else ici, pas sûr de la syntaxe
            nodes[nodesCount] = this;
            nodesCount++;
            if (this->right) {
                this->right->inorderTravel(nodes, nodesCount);
            }
            }
        }

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        if (this->isLeaf()) {
            nodes[nodesCount] = this;
            nodesCount++;
        }
        else {
            nodes[nodesCount] = this;
            nodesCount++;
            if (this->left) {
                this->left->preorderTravel(nodes, nodesCount);
            }
            if (this->right) {
                this->right->preorderTravel(nodes, nodesCount);
            }
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if (this->isLeaf()) {
            nodes[nodesCount] = this;
            nodesCount++;
        }
        else {
            if (this->left) {
                this->left->postorderTravel(nodes, nodesCount);
            }
            if (this->right) {
                this->right->postorderTravel(nodes, nodesCount);
            }
            nodes[nodesCount] = this;
            nodesCount++;
        }
	}

    Node* find(int value) {
        // find the node containing value
        // pas besoin de regarder si on a une feuille ou autre vu que les 3 cas gèrent déjà cela, par contre il n'y a pas de cas où la value demandée n'est pas dans l'arbre
        if (this->value == value) {
            return this;
        }
        else if (this->value < value) {
            if (this->right) {
                this->right->find(value);
            }
        }
        else {
            if (this->left) {
                this->left->find(value);
            }
        }
    }

    //il faut faire deux fonctions auxiliaires : l'une pour la rotation à droite et l'autre pour la rotation à gauche. Une fonction pour vérifier qu'un arbre est équilibré sera également la bienvenue.
    void rotationRight() {
        //rotation à droite autour du fils gauche de la racine de a
        //variables tampon utiles pour ne pas écraser des pointeurs inutilement
        //fdp : fils droit petit
        SearchTreeNode* fdp = this->left->right;
        SearchTreeNode* vieille_racine = this;
        //on met le fils droit de l'ancien fils gauche comme nouveau fils gauche du nouveau fils droit (donc de la racine de départ)
        vieille_racine->left = fdp;
        //on met le fils gauche de la racine à la racine
        *this = *(this->left);
        //on remplace le fils droit de la nouvelle racine par l'ancienne racine modifiée
        this->right = vieille_racine;
    }

    void rotationLeft() {
        //fgg : fils gauche grand
        SearchTreeNode* fgg = this->right->left;
        SearchTreeNode* vieille_racine = this;
        vieille_racine->right = fgg;
        *this = *(this->right);
        this->left = vieille_racine;
    }

    void equilibrageAVL() {
        //fonction qui équilibre un avl
        //cas d'arrêts
        //on ne vérifie nulle part que les noeuds existent bien car height prend déjà cela en compte
        int f = this->right->height() - this->left->height();
        if (f == 0 || f == 1 || f == -1) {
            return;
        }
        else {
            if (f == 2) {
                int f_r = this->right->right->height() - this->right->left->height();
                if (f_r == -1) {
                    this->rotationRight();
                    this->rotationLeft();
                } 
                else if (f_r == 1) {
                    this->rotationLeft();
                }
            }
            else if (f == -2) {
                int f_l = this->left->right->height() - this->left->right->height();
                if (f_l == -1) {
                    this->rotationRight();
                }
                else if (f_l == 1) {
                    this->rotationLeft();
                    this->rotationRight();
                }
            }
            //maintenant on fait les cas où la récursivité est nécessaire
            else if (f > 2) {
                this->right->equilibrageAVL();
            }
            else {
                this->left->equilibrageAVL();
            }
        }
    }

    void insertNumberBalanced(int value) {
        //cette fonction sert juste à lancer comme il faut nos fonctions récursives précédentes
        this->insertNumber(value);
        this->equilibrageAVL();
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
