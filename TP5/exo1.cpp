#include <tp5.h>
#include <QApplication>
#include <time.h>
#include <iostream>

MainWindow* w = nullptr;


std::vector<string> TP5::names(
{
    "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
    "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
    "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
    "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
    "Fanny", "Jeanne", "Elo"
});


int HashTable::hash(std::string element)
{
    // use this->size() to get HashTable size
    //on utilise quoiqu'il arrive le reste car si le codage ascii de element[0] est <this->size() il retourne la valeur de (int)element[0]
    return ((int)element[0])%this->size();
    
}

void HashTable::insert(std::string element)
{
    // use (*this)[i] or this->get(i) to get a value at index i
    this->get(this->hash(element)) = element; 
}

/**
 * @brief buildHashTable: fill the HashTable with given names
 * @param table table to fill
 * @param names array of names to insert
 * @param namesCount size of names array
 */
void buildHashTable(HashTable& table, std::string* names, int namesCount)
{
    for (int i = 0; i<namesCount; i++) {
        table.insert(names[i]);
    }
}

bool HashTable::contains(std::string element)
{
    // Note: Do not use iteration (for, while, ...)
    return element == this->get(this->hash(element));
}

//versions avec des listes chaînées
//on reprend les listes chaînées implementées lors du tp1

struct Noeud{
    std::string donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

void initialise(Liste* liste)
{
    liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier == nullptr) {
        return true;
    }
    return false;
}

void ajoute(Liste* liste, std::string valeur)
{
    Noeud* nv = new Noeud;
    nv->donnee = valeur;
    nv->suivant = nullptr;

    if (!est_vide(liste)) {
        //on va au dernier élément et on rajoute
        Noeud* tmp = liste->premier;
        while (tmp->suivant) {
            tmp = tmp->suivant;
        }
        tmp->suivant = nv;
    }
    else {
        liste->premier = nv;
    }
}

std::string recupere(const Liste* liste, int n)
{
    if (!est_vide(liste)) {
        int i = 0;
        Noeud* tmp = liste->premier;
        while (i<n && tmp->suivant) {
            i++;
            tmp = tmp->suivant;
        }
        if (i<n) {
            std::cout << "n trop grand" /*cas où n est en dehors de la liste*/;
        }
        else {
            return tmp->donnee;
        }
    }
    else {
        std::cout << "La liste est vide";
    }
    return "\0";
}

int cherche(const Liste* liste, std::string valeur)
{
    //il y a deux cas où on renvoie -1 : la liste est vide et la liste n'est pas vide mais on ne trouve pas la valeur
    int i = 0;
    Noeud* tmp = liste->premier;
    if (!est_vide(liste)) {
        while (tmp->suivant && valeur!=tmp->donnee) {
            i++;
            tmp = tmp->suivant;
        }
        if (valeur == tmp->donnee) {
            return i;
        }
        else {
            return -1;
        }
    }
    else {
        std::cout << "La liste est vide";
        return -1;
    }
}

void stocke(Liste* liste, int n, std::string valeur)
{
    int i = 0;
    Noeud* tmp = liste->premier;
    if (!est_vide(liste)) {
        while (i<n && tmp->suivant) {
            i++;
            tmp = tmp->suivant;
        }
        if (i<n) {
            std::cout << "n trop grand";
        }
        else {
            tmp->donnee = valeur;
        }
    }
}

//hash ne change pas
//problème quand on définit les 2 prochaines fonctions car ne sont pas comprises dans le .h donc ça ne marche pas à la compilation
void HashTable::insertList(std::string element)
{
    int place = this->hash(element);
    ajoute(cherche(this->get(place)), element);
}

bool HashTable::containsList(std::string element)
{
    //si -1 est retourné par cherche, cela veut dire que element n'était pas dans la liste
    return cherche(this->get(this->hash(element)), element) != -1;
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 10;
	w = new HashWindow();
	w->show();

	return a.exec();
}
