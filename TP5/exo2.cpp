#include <QApplication>
#include <QString>
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <string>

#include <tp5.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

//fonction importée depuis le tp1
long power(long value, long n)
{
   if (n == 0) {
        return 1;
    }
    else if (n == 1) {
        return value;
    }
    else {
        if (n%2 == 0) {
            long y = power(value, n/2);
            return y*y;
        }
        else {
            long y = power(value, n/2);
            return value*y*y;
        }
    }
}

std::vector<string> TP5::names(
{
    "Yolo", "Anastasiya", "Clement", "Sirine", "Julien", "Sacha", "Leo", "Margot",
    "JoLeClodo", "Anais", "Jolan", "Marie", "Cindy", "Flavien", "Tanguy", "Audrey",
    "Mr.PeanutButter", "Bojack", "Mugiwara", "Sully", "Solem",
    "Leo", "Nils", "Vincent", "Paul", "Zoe", "Julien", "Matteo",
    "Fanny", "Jeanne", "Elo"
});

unsigned long int hash(string key)
{
    unsigned long int res = 0;
    for (int i = 0; i<key.size(); i++) {
        res += ((int)key[i])*power(128, key.size()-i-1);
    }
    return res%128;//on renvoie le modulo de la grosse somme 
}

struct MapNode : public BinaryTree
{

    string key;
    unsigned long int key_hash;

    int value;

    MapNode* left;
    MapNode* right;

    MapNode(string key, int value) : BinaryTree (value)
    {
        this->key = key;
        this->value = value;
        this->key_hash = hash(key);

        this->left = this->right = nullptr;
    }

    /**
     * @brief insertNode insert a new node according to the key hash
     * @param node
     */
    void insertNode(MapNode* node)
    {
        //on va faire comme si les arbres sont forcément des arbres binaires de recherche
        //on ne se soucie pas ici de l'équilibrage de l'arbre
        if (!this) {
            this->key = node->key;
            this->value = node->value;
            this->key_hash = node->key_hash;
            this->left = this->right = nullptr;
        }
        else {
            if (node->key_hash < this->key_hash) {
                if (this->left) {
                    this->left->insertNode(node);
                }
                else {
                    this->left = node;
                }
            } 
            else {
                if (this->right) {
                    this->right->insertNode(node);
                }
                else {
                    this->right = node;
                }
            }
        }
    }

    void insertNode(string key, int value)
    {
        this->insertNode(new MapNode(key, value));
    }

    virtual ~MapNode() {}
    QString toString() const override {return QString("%1:\n%2").arg(QString::fromStdString(key)).arg(value);}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}

    int getKey(string key)
    {
        //ce n'est pas super efficace car on recalcule n, mais la version récursive me paraissait plus naturelle
        long unsigned int n = hash(key);
        //cas d'arrêt si on a trouvé la bonne valeur
        if (!this) {
            return 0;
        }
        else {
            if (n == this->key_hash) {
                return this->value;
            }
            //les cas d'arrêt où on n'a pas trouvé la bonne valeur renvoient 0 et sont directement dans les cas récursifs
            else if (n < this->key_hash) {
                if (this->left) {
                    return this->left->getKey(key);
                }
                else {
                    return 0;
                }
            }
            else {
                if (this->right) {
                    return this->right->getKey(key);
                }
                else {
                    return 0;
                }
            }
        }
    }
    MapNode* root;
};

struct Map
{
    Map() {
        this->root = nullptr;
    }

    /**
     * @brief insert create a node and insert it to the map
     * @param key
     * @param value
     */
    void insert(string key, int value)
    {
        MapNode* node = new MapNode(key, value);
        //on utilise la fonction définit précédemment pour insérer le noeud qu'on vient de créer
        this->root->insertNode(node);
    }

    /**
     * @brief get return the value of the node corresponding to key
     * @param key
     * @return
     */

    int get(string key) 
    {
        return this->root->getKey(key);
    }
    MapNode* root;
};


int main(int argc, char *argv[])
{
    srand(time(NULL));
	Map map;
    std::vector<std::string> inserted;

    map.insert("Yolo", 20);
    for (std::string& name : TP5::names)
    {
        if (rand() % 3 == 0)
        {
            map.insert(name, rand() % 21);
            inserted.push_back(name);
        }
    }

    printf("map[\"Margot\"]=%d\n", map.get("Margot"));
    printf("map[\"Jolan\"]=%d\n", map.get("Jolan"));
    printf("map[\"Lucas\"]=%d\n", map.get("Lucas"));
    printf("map[\"Clemence\"]=%d\n", map.get("Clemence"));
    printf("map[\"Yolo\"]=%d\n", map.get("Yolo"));
    printf("map[\"Tanguy\"]=%d\n", map.get("Tanguy"));

    printf("\n");
    for (size_t i=0; i<inserted.size()/2; i++)
        printf("map[\"%s\"]=%d\n", inserted[i].c_str(), map.get(inserted[i]));


    std::cout.flush();

    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new MapWindow(*map.root);
    w->show();
    return a.exec();
}
