#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChildIndex(int nodeIndex)
{
    return 2*nodeIndex + 1;
}

int Heap::rightChildIndex(int nodeIndex)
{
    return 2*nodeIndex + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    this->set(i, value);
    while (i>0 && this->get(i) > this->get((i-1)/2)) {
        this->swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
    //pas sûr que ce cas là soit vraiment utile
    if (nodeIndex >= heapSize) {
        return;
    }
    if (this->leftChildIndex(nodeIndex) < heapSize) {
        if (this->get(nodeIndex) < this->get(this->leftChildIndex(nodeIndex))) {
            i_max = this->leftChildIndex(nodeIndex);
        }
    }
    if (this->rightChildIndex(nodeIndex) < heapSize) {
        //on met get(i_max) car on a potentiellement mis à jour i_max dans le if juste avant
        if (this->get(i_max) < this->get(this->rightChildIndex(nodeIndex))) {
            i_max = this->rightChildIndex(nodeIndex);
        }
    }
    if (i_max != nodeIndex) {
        this->swap(i_max, nodeIndex);
        this->heapify(heapSize, i_max);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for(int i = 0; i<numbers.size(); i++) {
        this->insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    for (int i = this->size()-1; i>=0; i--) {
        this->swap(0, i);
        this->heapify(i, 0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
