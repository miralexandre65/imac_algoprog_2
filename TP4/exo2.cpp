#include <time.h>
#include <stdio.h>

#include <QApplication>
#include <QDebug>

#include "tp3.h"
#include "tp4.h"
#include "tp4_exo2.h"
#include "HuffmanNode.h"

_TestMainWindow* w1 = nullptr;
using std::size_t;
using std::string;

void processCharFrequences(string data, Array& frequences);
void buildHuffmanHeap(const Array& frequences, HuffmanHeap& priorityMinHeap, int& heapSize);
HuffmanNode* makeHuffmanSubTree(HuffmanNode* rightNode, HuffmanNode* leftNode);
HuffmanNode* buildHuffmanTree(HuffmanHeap& priorityMinHeap, int heapSize);

string huffmanEncode(const string& toEncode, HuffmanNode* huffmanTree);
string huffmanDecode(const string& toDecode, const HuffmanNode& huffmanTreeRoot);


void main_function(HuffmanNode*& huffmanTree)
{
    string data = "Ouesh, bien ou bien ? Ceci est une chaine de caracteres sans grand interet";

    // this array store each caracter frequences indexed by their ascii code
    Array characterFrequences(256);
    characterFrequences.fill(0);
    // this array store each caracter code indexed by their ascii code
    string characterCodes[256];
    HuffmanHeap priorityMinHeap;
    int heapSize = 0;

    processCharFrequences(data, characterFrequences);
    displayCharacterFrequences(characterFrequences);
    buildHuffmanHeap(characterFrequences, priorityMinHeap, heapSize);
    qDebug() << priorityMinHeap.toString().toStdString().c_str();

    huffmanTree = buildHuffmanTree(priorityMinHeap, heapSize);
    huffmanTree->processCodes("");
    string encoded = huffmanEncode(data, huffmanTree);
    string decoded = huffmanDecode(encoded, *huffmanTree);

    qDebug("Encoded: %s\n", encoded.c_str());
    qDebug("Decoded: %s\n", decoded.c_str());
}


void processCharFrequences(string data, Array& frequences)
{
    /**
      * Fill `frequences` array with each caracter frequence.
      * frequences is an array of 256 int. frequences[i]
      * is the frequence of the caracter with ASCII code i
     **/

    // Your code
    for (int i = 0; i<data.length(); i++)
        frequences[(int)data[i]]+=1;
}

void HuffmanHeap::insertHeapNode(int heapSize, HuffmanNode* newNode)
{
    /** 
      * Insert a HuffmanNode into the lower heap. A min-heap put the lowest value
      * as the first cell, so check the parent should be lower than children.
      * Instead of storing int, the cells of HuffmanHeap store HuffmanNode*.
      * To compare these nodes use their frequences.
      * this->get(i): HuffmanNode*  <-> this->get(i)->frequences
      * you can use `this->swap(firstIndex, secondIndex)`
     **/

    // Your code
    int i = heapSize;
    this->set(i, newNode);
    this->get((i-1)/2)->right = newNode;
    while (i>0 && this->get(i)->frequences < this->get((i-1)/2)->frequences) {
        //on crée nos variables tampons
        HuffmanNode* p_left = this->get((i-1)/2)->left;
        HuffmanNode* f_left = this->get(i)->left;
        HuffmanNode* f_right = this->get(i)->right;

        //on échanges les fils droit et gauche des noeuds concernés afin que chaque noeud final ait le bon fils droit et gauche
        this->get(i)->left = p_left;
        this->get(i)->right = this->get((i-1)/2);
        this->get((i-1)/2)->left = f_left;
        this->get((i-1)/2)->right = f_right;
        this->swap(i, (i-1)/2);
        i = (i-1)/2;
    }
}

void buildHuffmanHeap(const Array& frequences, HuffmanHeap& priorityMinHeap, int& heapSize)
{
    /**
      * Do like Heap::buildHeap. Use only non-null frequences
      * Define heapSize as numbers of inserted nodes
      * allocate a HuffmanNode with `new`
     **/

    // Your code
    for (int i = 0; i<frequences.size(); i++) {
        if (frequences[i] != 0) {
            HuffmanNode* node = new HuffmanNode(frequences[i]);
            heapSize++;
            priorityMinHeap.insertHeapNode(heapSize, node);
        }
    }
}

void HuffmanHeap::heapify(int heapSize, int nodeIndex)
{
    /**
      * Repair the heap starting from nodeIndex. this is a min-heap,
      * so check the parent should be lower than children.
      * this->get(i): HuffmanNode*  <-> this->get(i)->frequences
      * you can use `this->swap(firstIndex, secondIndex)`
     **/
    // Your code
    //on va utiliser la même méthode que pour un tas max, à savoir : regarder qui a la fréquence la plus petite des trois éléments et les bouger si besoin 
    int i_min = nodeIndex;
    if (nodeIndex >= heapSize) {
        return;
    }
    //on regarde le fils droit
    else if (2*nodeIndex + 2 < heapSize) {
        if (this->get(2*nodeIndex + 2)->frequences < this->get(nodeIndex)->frequences) {
            i_min = 2*nodeIndex + 2;
        }
        else {
            HuffmanNode* f_left = this->get(i_min)->left;
            HuffmanNode* f_right = this->get(i_min)->right;
            HuffmanNode* p_left = this->get(nodeIndex)->left;

            //on échange
            this->get(i_min)->left = p_left;
            this->get(i_min)->right = this->get(nodeIndex);
            this->get(nodeIndex)->left = f_left;
            this->get(nodeIndex)->right = f_right;
            this->swap(i_min, nodeIndex);
            //on relance
            this->heapify(heapSize, i_min);
        }
    }
    else if (2*nodeIndex + 1 < heapSize) {
        if (this->get(2*nodeIndex + 1)->frequences < this->get(i_min)->frequences) {
            i_min = 2*nodeIndex + 1;
        }
        else {
            HuffmanNode* f_left = this->get(i_min)->left;
            HuffmanNode* f_right = this->get(i_min)->right;
            HuffmanNode* p_left = this->get(nodeIndex)->left;

            //on échange
            this->get(i_min)->left = p_left;
            this->get(i_min)->right = this->get(nodeIndex);
            this->get(nodeIndex)->left = f_left;
            this->get(nodeIndex)->right = f_right;
            this->swap(i_min, nodeIndex);
            //on relance
            this->heapify(heapSize, i_min);
        }
    }
}


HuffmanNode* HuffmanHeap::extractMinNode(int heapSize)
{
    /**
      * Extract the first cell, replace the first cell with the last one and
      * heapify the heap to get a new well-formed heap without the returned cell
      * you can use `this->swap`
     **/

    // Your code
    HuffmanNode* min = this->get(0);
    //échange des deux noeuds proprement (enfin, je crois...)
    HuffmanNode* f_left = this->get(0)->left;
    HuffmanNode* f_right = this->get(0)->right;
    this->get(heapSize-1)->left = f_left;
    this->get(heapSize-1)->right = f_right;
    this->swap(0, heapSize-1);
    //on va supprimer le dernier noeud du tas car on a échangé
    if (this->get((heapSize-1)/2)->right->character == min->character) {
        this->get((heapSize-1)/2)->right = nullptr;
    }
    else if (this->get((heapSize-1)/2)->left->character == min->character) {
        this->get((heapSize/1)/2)->left = nullptr;
    }
    heapSize--;
    this->heapify(0, heapSize);
    return min;
}

HuffmanNode* makeHuffmanSubTree(HuffmanNode* rightNode, HuffmanNode* leftNode)
{
    /**
     * Make a subtree (parent + 2 children) with the given 2 nodes.
     * These 2 characters will be the children of a new parent node which character is '\0'
     * and frequence is the sum of the 2 children frequences
     * Return the new HuffmanNode* parent
     **/
    // Your code
    HuffmanNode* node = new HuffmanNode(rightNode->frequences + leftNode->frequences);
    node->character = '\0';
    node->left = leftNode;
    node->right = rightNode;
    return node;
}

HuffmanNode* buildHuffmanTree(HuffmanHeap& priorityMinHeap, int heapSize)
{
    /**
      * Build Huffman Tree from the priorityMinHeap, pick nodes from the heap until having
      * one node in the heap. For every 2 min nodes, create a subtree and put the new parent
      * into the heap. The last node of the heap is the HuffmanTree;
      * use extractMinNode()
     **/

    // Your code
    while (heapSize != 1) {
        HuffmanNode* first = priorityMinHeap.extractMinNode(heapSize);
        heapSize--;
        HuffmanNode* second = priorityMinHeap.extractMinNode(heapSize);
        heapSize--;
        HuffmanNode* parent = makeHuffmanSubTree(first, second);
        priorityMinHeap.insertHeapNode(heapSize, parent);
        heapSize++;
    }
    return priorityMinHeap.extractMinNode(heapSize);
}

void HuffmanNode::processCodes(const std::string& baseCode)
{
    /**
      * Travel whole tree of HuffmanNode to determine the code of each
      * leaf/character.
      * Each time you call the left child, add '0' to the baseCode
      * and each time call the right child, add '1'.
      * If the node is a leaf, it takes the baseCode.
     **/

    //trouve le code de chaque caractère et change leur code pour y mettre le code trouver dans l'arbre
    //il y aura donc plusieurs appels à la fonction qui seront indépendants les uns des autres
    //le seul cas d'arrêt est quand on a une feuille
    if (!this->left && !this->right) {
        this->code = baseCode;
    }
    else {
        if (this->left) {
            ///!\ attention à ne pas passer baseCode+='0' en argument, car ici on modifie le string, ce que l'on n'a pas le droit de faire à cause du const, alors que renvoyer un nouveau string qui est baseCode + str, ça c'est ok. Retourner voir sur internet si je ne sais plus pourquoi on fait ça comme ça. 
            this->left->processCodes(baseCode + '0');
        }
        if (this->right) {
            this->right->processCodes(baseCode + '1');
        }
    } 
}

void HuffmanNode::fillCharactersArray(std::string charactersCodes[])
{
    /**
      * Fill the string array with all nodes codes of the tree
      * It store a node into the cell corresponding to its ascii code
      * For example: the node describing 'O' should be at index 79
     **/
    if (!this->left && !this->right)
        charactersCodes[this->character] = this->code;
    else {
        if (this->left)
            this->left->fillCharactersArray(charactersCodes);
        if (this->right)
            this->right->fillCharactersArray(charactersCodes);
    }
}

string huffmanEncode(const string& toEncode, HuffmanNode* huffmanTree)
{
    /**
      * Encode a string by using the huffman compression.
      * With the huffmanTree, determine the code for each character
     **/

    // Your code
    std::string charactersCodes[256]; // array of 256 huffman codes for each character
    huffmanTree->fillCharactersArray(charactersCodes);
    string encoded = "";
    //on a juste à aller chercher les codes dans le tableau charactersCodes
    for (int i = 0; i<toEncode.length(); i++) {
        encoded+=charactersCodes[toEncode[i]];//on ajoute le code de chaque lettre dans la version encodée du mot
    }
    return encoded;
}


string huffmanDecode(const string& toDecode, const HuffmanNode& huffmanTreeRoot)
{
    /**
      * Use each caracters of toDecode, which is '0' either '1',
      * to travel the Huffman tree. Each time you get a leaf, get
      * the decoded character of this node.
     **/
    // Your code
    string decoded = "";
    const HuffmanNode* elem = &huffmanTreeRoot;
    for (int i = 0; i<toDecode.length(); i++) {
        if (!elem->left && !elem->right) {
            decoded+=elem->character;
            elem = &huffmanTreeRoot;
        }
        else {
            if (toDecode[i] == 0) {
                elem = (elem->left);
            }
            else {
                elem = (elem->right);
            }
        }
    }
    return decoded;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Array::wait_for_operations = false;
    w1 = new HuffmanMainWindow(main_function);
    w1->show();
    return a.exec();
}
