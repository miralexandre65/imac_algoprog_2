#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	// initialisation
    if (origin.size() <= 1) {
        return;
     }

	Array& first = w->newArray(origin.size()/2);
	Array& second = w->newArray(origin.size()-first.size());
	
	// split
    for (int i = 0; i<origin.size()/2; i++) {
        first.set(i, origin[i]);
    }
    for (int i = first.size(); i<origin.size(); i++) {
        second.set(i-first.size(), origin[i]);
    }

	// recursiv splitAndMerge of lowerArray and greaterArray
    if (first.size()>1) {
        splitAndMerge(first);
    }
    if (second.size()>1) {
        splitAndMerge(second);
    }

	// merge
    merge(first, second, origin);

}

void merge(Array& first, Array& second, Array& result)
{
    int i = 0;
    int j = 0;
    while(i<first.size() && j<second.size()) {
        if (first[i] <= second[j]) {
            result.set(i+j, first[i]);
            i++;
        }
        else {
            result.set(i+j, second[j]);
            j++;
        } 
    }

    if (i == first.size()) {
        for (int k = j; k<second.size(); k++) {
            result.set(i+k, second[k]);
        }
    } 
    else if (j == second.size()){
        for (int k = i; k<first.size(); k++) {
            result.set(k+j, first[k]);
        }
    }

}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
