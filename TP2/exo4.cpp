#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
    // stop statement = condition + return (return stop the function even if it does not return anything)
    if (size <= 1) {
        return;
    }

    Array& lowerArray = w->newArray(size);
    Array& greaterArray= w->newArray(size);
    int lowerSize = 0, greaterSize = 0; // effectives sizes

    int pivot = size/2;
    int value_pivot = toSort[pivot];

    // split
    for (int i = 0; i<size; i++) {
        //attention à regarder qu'on distingue le cas où i==pivot car sinon on ne peut pas traiter les doublons du pivot
        if (i == pivot) {}
        else {
            if (toSort[i] < toSort[pivot]) {
                lowerArray.set(lowerSize, toSort[i]);
                lowerSize++;
            }
            else if (toSort[i] >= toSort[pivot]) {
                greaterArray.set(greaterSize, toSort[i]);
                greaterSize++;
            }
        }
    }

    // recursiv sort of lowerArray and greaterArray
    if (lowerSize > 1) {
        recursivQuickSort(lowerArray, lowerSize);
    }
    if (greaterSize > 1) {
        recursivQuickSort(greaterArray, greaterSize);
    }

    // merge
    // attention à bien se caler par rapport au lowerSize et pas au pivot, il n'est pas toujours au milieu du tableau car on ne sait pas combien d'éléments sont inférieurs à lui, donc il faut faire en fonction de lowerSize
    for (int i = 0; i<size; i++) {
        if (i == lowerSize) {
            toSort.set(i, value_pivot);
        }
        else if (i<lowerSize) {
            toSort.set(i, lowerArray[i]);
        }
        else {
            toSort.set(i, greaterArray[i-lowerSize-1]);
        }
    }
}

void quickSort(Array& toSort){
    recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    uint elementCount=20;
    MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
    w->show();

	return a.exec();
}
