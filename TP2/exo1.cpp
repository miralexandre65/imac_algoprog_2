#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

int getMin(Array& toSort, int begin) {
    int res = begin;
    if (toSort.size() != 0 && begin < toSort.size()-1) {
        for (int i = begin; i<toSort.size(); i++) {
            if (toSort[i]<toSort[res]) {
                res = i;
            }
        }
    }
    return res;
}

void selectionSort(Array& toSort){
	// selectionSort
    for (int i = 0; i<toSort.size()-1; i++) {
        int min = getMin(toSort, i);
        toSort.swap(i, min);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
