#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);

long power(long value, long n)
{
    Context _("power", value, n); // do not care about this, it allow the display of call stack

    // your code
    /*if (n==0) {
        return 1;
    }
    if (n==1) {
        return value;
    }
    // return_and_display shows the result in the window and returns it
    return_and_display(value*power(value, n-1));*/ //version naïve en O(n)

    //version plus efficace en O(log(n)) qui utilise la division euclidienne par 2
    if (n == 0) {
        return 1;
    }
    else if (n == 1) {
        return value;
    }
    else {
        if (n%2 == 0) {
            long y = power(value, n/2);
            return_and_display(y*y);
        }
        else {
            long y = power(value, n/2);
            return_and_display(value*y*y);
        }
    }
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new PowerWindow(power); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}
