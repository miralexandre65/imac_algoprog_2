#include "tp1.h"
#include <QApplication>
#include <time.h>

#define return_and_display(result) return _.store(result);

int fibonacci(int value)
{
    Context _("fibonacci", value); // do not care about this, it allow the display of call stack

    // your code
    //c'est seulement une version naïve, on pourrait implémenter une version efficace mais il faudrait faire une fonction intermédiaire qui renverrait un couple et qui permettrait de ne pas faire des calculs inutiles
    if (value == 0) {
        return 0;
    }
    if (value == 1 || value == 2) {
        return 1;
    }
    else {
        return_and_display(fibonacci(value-1) + fibonacci(value-2));
    }
    //c'est vraiment très inefficace, même avec value = 12 c'est très très long
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);  // create a window manager
    MainWindow::instruction_duration = 400;  // make a pause between instruction display
    MainWindow* w = new FibonacciWindow(fibonacci); // create a window for this exercice
    w->show(); // show exercice

    return a.exec(); // main loop while window is opened
}
