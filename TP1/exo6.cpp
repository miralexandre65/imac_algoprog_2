#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    // your code
    int taille;//taille effective (nb d'éléments dans le tableau)
    int capacite;//taille totale du tableau
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if (liste->premier == nullptr) {
        return true;
    }
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* nv = new Noeud;
    nv->donnee = valeur;
    nv->suivant = nullptr;

    if (!est_vide(liste)) {
        //on va au dernier élément et on rajoute
        Noeud* tmp = liste->premier;
        while (tmp->suivant) {
            tmp = tmp->suivant;
        }
        tmp->suivant = nv;
    }
    else {
        liste->premier = nv;
    }
}
//problème dans affiche et récupère avec le const je pense
void affiche(const Liste* liste)
{
    if (!est_vide(liste)) {
        Noeud* tmp = liste->premier;
        while (tmp) {
            cout << tmp->donnee << " ";
            tmp = tmp->suivant;
        }
    } 
    else {
        cout << "La liste est vide";
    }
}

int recupere(const Liste* liste, int n)
{
    if (!est_vide(liste)) {
        int i = 0;
        Noeud* tmp = liste->premier;
        while (i<n && tmp->suivant) {
            i++;
            tmp = tmp->suivant;
        }
        if (i<n) {
            cout << "n trop grand" /*cas où n est en dehors de la liste*/;
        }
        else {
            return tmp->donnee;
        }
    }
    else {
        cout << "La liste est vide";
    }
    return 0;//recupere renvoie 0 dans quel cas ?
}

int cherche(const Liste* liste, int valeur)
{
    //il y a deux cas où on renvoie -1 : la liste est vide et la liste n'est pas vide mais on ne trouve pas la valeur
    int i = 0;
    Noeud* tmp = liste->premier;
    if (!est_vide(liste)) {
        while (tmp->suivant && valeur!=tmp->donnee) {
            i++;
            tmp = tmp->suivant;
        }
        if (valeur == tmp->donnee) {
            return i;
        }
        else {
            return -1;
        }
    }
    else {
        cout << "La liste est vide";
        return -1;
    }
}

void stocke(Liste* liste, int n, int valeur)
{
    int i = 0;
    Noeud* tmp = liste->premier;
    if (!est_vide(liste)) {
        while (i<n && tmp->suivant) {
            i++;
            tmp = tmp->suivant;
        }
        if (i<n) {
            cout << "n trop grand";
        }
        else {
            tmp->donnee = valeur;
        }
    }
}


void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->taille < tableau->capacite) {
        //il y a encore de la place dans le tableau
        tableau->donnees[tableau->taille] = valeur;
        tableau->taille++;
    }
    else {
        tableau->capacite *= 2;//on agrandit la capacité totale pour pouvoir stocker les nouvelles données
        //ce serait plus simple d'allouer la mémoire avec malloc() et d'utiliser realloc() pour ensuite agrandir le tableau (il faudrait en revanche utiliser size_of), mais ici on va faire avec new
        int* tmp = new int[tableau->capacite];
        for (int i = 0; i < tableau->taille; i++) {
            tmp[i] = tableau->donnees[i];
        }
        tmp[tableau->taille] = valeur;       
        tableau->taille++;
        delete[] tableau->donnees;//on libère la mémoire du tableau qui ne nous sert plus
        tableau->donnees = tmp;
    }
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->taille = 0;
    tableau->donnees = new int[tableau->capacite];
}

bool est_vide(const DynaTableau* tableau)
{
    //on regarde si la taille effective du tableau est nulle, dans le cas contraire le tableau n'est pas nul
    return tableau->taille == 0;

}

void affiche(const DynaTableau* tableau)
{
    if (!est_vide(tableau)) {
        for (int i = 0; i < tableau->taille; i++) {
            cout << tableau->donnees[i] << " ";
        }
    }
    else {
        cout << "Le tableau est vide";
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    if (!est_vide(tableau) && n <= tableau->taille) {
        return tableau->donnees[n];//il y a une ambiguité entre l'élément n et la n ème valeur...
    }
    else {
        cout << "Le n demandé est trop grand.";
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    if (!est_vide(tableau)) {
        int i = 0;
        while (tableau->donnees[i] != valeur && i < tableau->taille) {
            i++;
        }
        if (tableau->donnees[i] == valeur) {
            return i;
        }
        else {
            return -1;
        }
    }
    else {
        cout << "Le tableau est vide.";
        return -1;
    }
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (!est_vide(tableau) && n <= tableau->taille) {
        tableau->donnees[n-1] = valeur; 
    } 
    else {
        cout << "Le tableau est vide ou n est trop grand.";
    }
}

//pour les files, on va ajouter les valeurs à la fin des structures et pour les piles on va ajouter au début, ce sera plus simple pour ensuite enlever les éléments dans chaque cas

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur); 
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (!est_vide(liste)) {
        Noeud* tmp = liste->premier->suivant;;
        cout << liste->premier->donnee;
        //delete liste->premier;
        liste->premier = tmp;
        }
    else {
        cout << "La file est vide.";
    }
    return 0;
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    if (!est_vide(liste)) {
        //attention à ne pas delete des pointeurs de façon débile car après il y a des double free error
        //attention quand on veut faire une variable tampon pour un pointeur avec une structure à créer une nouvelle adresse car sinon on modifiera quand même la case mémoire de base car deux variables vont pointer sur la même adresse
        Noeud* tmp = new Noeud;//quoi qu'il arrive, utiliser une variable tampon est la bonne solution quand on veut modifier ce genre de structure
        // Noeud* tmp = liste->premier : ne pas mettre ça car les autres instructions vont alors modifier tmp aussi comme c'est un pointeur vers la case
        tmp->donnee = valeur;
        tmp->suivant = liste->premier;
        //delete liste->premier; : si on met ça on segfault, donc c'est idiot, s'en souvenir (en gros ça supprime le tmp->suivant juste au-dessus, je ne vois vraiment pas ce qui m'a pris en mettant ça)
        liste->premier = tmp;
    }
    else {
        Noeud* nv = new Noeud;
        nv->donnee = valeur;
        nv->suivant = nullptr;
        liste->premier = nv;
    }
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if (!est_vide(liste)) {
        Noeud* tmp = liste->premier->suivant;;
        cout << liste->premier->donnee;
        //delete liste->premier;
        liste->premier = tmp;
    }
    else {
        cout << "La pile est vide.";
    }   
    return 0;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    cout << "\n";
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans le tableau à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    cout << "\n";
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y ass un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
